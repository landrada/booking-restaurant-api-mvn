package booking_restaurant_api_mvn.booking_restaurant_api_mvn.suma;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SumaTest {

	Suma sum = new Suma();

	@Before
	public void before() {
		System.out.println("before");
	}

	@Test
	public void sumaTest() {
		System.out.println("sumaTest");
		int sumTest = sum.suma(1, 1);
		int resultadoEsperado = 2;
		assertEquals(resultadoEsperado, sumTest);
	}

	@After
	public void after() {
		System.out.println("after");
	}

}
